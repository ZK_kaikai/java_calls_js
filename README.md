# java调用js

#### 介绍
java使用自身的js引擎调用js运行的测试demo

#### 软件架构
java从jdk6开始支持js运行，自带的js解释引擎，从JDK8开始，Nashorn引擎开始取代Rhino (jdk6、7中)成为java的嵌入式js引擎,它将js代码编译为java字节码，与先前的Rhino的实现相比，性能提升了2到10倍

#### 特性
###### 1.java运行js脚本遵循ECMAScript规范
###### 2.可以把java对象作为参数传入js函数处理，并可返回传入的java对象
###### 3.js脚本中可引入java的类，然后可以执行在js中使用java静态方法、创建java对象、处理java对象属性、返回java对象等操作，原理是利用 Java.type
###### 4.js脚本可以保存传入或创建的java对象，然后在其他js函数中调用
###### 5.js脚本可以创建、添加对象、遍历java中的Map、Set、List等集合，原理是利用  for each 
###### 6.js脚本中可继承java类，然后根据继承的类创建对象使用，原理是使用   Java.extend
#### 注意
###### 1.js中定义的函数方法名一定不能重复
###### 2.如果js调用了java的东西，一定要参数拼写和java的一致
###### 3.js中的方法可以比接口中定义的方法多，但接口类中的方法js中一定要定义，要不然无法获取js接口对象，导致无法运行
###### 4.json对象在js中处理，相当于也是把java对象传入js，也就是java的json对象不能和js的json对象互通，必须使用对应的方法互相转换，比如js中获取json对象使用 JSON.parse，js创建返回java的json对象就是引入对应的json类，然后使用该类构造方法创建
###### 5.一些前端特有的js函数不可以执行，比如alert、console、setTimeout等
#### 参考网址
###### https://www.cnblogs.com/top8/p/6207945.html
###### https://blog.csdn.net/jasnet_u/article/details/83475082
###### https://blog.csdn.net/u014174048/article/details/80899776
###### https://github.com/winterbe/nake
