
/* 测试简单的js运行 */
function execute(s1, s2) {
    if (!validate(s1, s2)) {
        return s2 + s1
    }
    return s1 + s2;
}
/* 测试把 java对象传入js处理 */

function executebean(s1) {
	/*java对象传入js的变量*/
	temp = s1;
	var tempstr = s1.getName()+":"+s1.getValue();
    return tempstr;
}
var  temp;
/* 测试java对象传入js的变量，然后通过函数返回该对象的属性 */
function execute1() {
    return temp.getOther();
}

/* 测试java对象传入js的变量，然后通过函数返回该对象 */
function execute2() {
	temp.setValue("987");
	temp.setOther("new Tag");
    return temp;
}

/* 测试把java对象传入js，js修改java对象属性，然后返回java对象 */
function execute3(TagC) {
	TagC.setName("bsdhbsd h");
	TagC.setValue("987");
	TagC.setOther("new Tag");
    return TagC;
}

/*测试json对象的处理，调用其他js函数的返回对象，获取对象属性的和设置对象属性*/
function execute4(json) {
	var abc = execute2();
	/*创建json对象*/
	var jsjo =  JSON.parse(json);
	print("el_json:"+json);
	print("el_jsjo.abc1:"+jsjo.abc);
	json.put("Value", abc.getValue());
	json.put("Name", abc.getName());
    return json;
}

/*先引入java对象的类*/
var MYzk = Java.type("youwin.zk.bean.MYzk");
var Tag = Java.type("youwin.zk.bean.Tag");

/* 测试在js中创建java对象返回 */
function execute5(){
	/*然后创建java对象*/
	var MYzkOBJ = new MYzk();
	var TagOBJ = new Tag();
	TagOBJ.setName("execute");
	TagOBJ.setValue("5");
	MYzkOBJ.setTag(TagOBJ);
	MYzkOBJ.setName(temp.getOther());
	return MYzkOBJ;
}


/*测试js调用java静态方法*/
function execute6(){
	var TestFun = Java.type("youwin.zk.mystatic.TestFun");
	return TestFun.Fun1();
}

/* 测试集合与For Each */
function execute7(){
	var ArrayList = Java.type('java.util.ArrayList');
	var list = new ArrayList();
	list.add('a');
	list.add('b');
	list.add('c');
	/*为了遍历集合和数组中的元素，Nashorn 引入了 for each 语句。这就像是 Java 的 for 循环一样。*/
	for each (var el in list) {
		print("js_el:"+el);  // a, b, c
	}
}

/* 测试MAP与For Each */
function execute8(){
	var map = new java.util.HashMap();
	map.put('foo', 'val1');
	map.put('bar', 'val2');

	for each (var e in map.keySet()) {
		print("js_key:"+e);  // foo, bar
	}

	for each (var e in map.values()) {
		print("js_value:"+e);  // val1, val2
	}
}

/* 测试java的数组传入js处理 */
function execute9(){
	var IntArray = Java.type("int[]");

	var array = new IntArray(5);
	array[0] = 5;
	array[1] = 4;
	array[2] = 3;
	array[3] = 2;
	array[4] = 1;

	try {
	    array[5] = 23;
	} catch (e) {
	    print("js_e:"+e.message);  // Array index out of range: 5
	}

	//js自带把字符串转int
	array[0] = "17";
	print("js_array1:"+array[0]);  // 17

	array[0] = "wrong type";
	print("js_array2:"+array[0]);  // 0

	array[0] = "17.3";
	print("js_array3:"+array[0]);  // 17
}


/* 测试js中对象继承java类型和js多线程 */
function execute10(){
	var Runnable = Java.type('java.lang.Runnable');
	var Thread = Java.type('java.lang.Thread');
	var Printer = Java.extend(Runnable, {
	    run: function() {
	    	/*调用静态方法，先休眠1秒*/
	    	Thread.sleep(1000);
	        print("js_t:"+'printed from a separate thread');
	        for(i = 0;i<10;i++){
	        	Thread.sleep(1000);
		        print("js_t:"+i+'printed from a separate thread');
	        }
	    }
	});


	new Thread(new Printer()).start();

	new Thread(function() {
	    print("js_t:"+'printed from another thread');
	}).start();
	
	/*休眠几秒,测试看js运行是不是同一个线程*/
	Thread.sleep(5000);

}

