package youwin.zk.bean;

public class Tag {
	private String name;
	private String value;
	private String Other;

	public String getOther() {
		return Other;
	}

	public void setOther(String other) {
		Other = other;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	@Override
	public String toString() {
		return "Tag [name=" + name + ", value=" + value + ", Other=" + Other + "]";
	}

}
