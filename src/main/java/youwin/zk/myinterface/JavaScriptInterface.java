package youwin.zk.myinterface;

import org.json.JSONObject;

import youwin.zk.bean.MYzk;
import youwin.zk.bean.Tag;
import youwin.zk.bean.TagC;

public interface JavaScriptInterface {
	/* 测试简单的js运行 */
	public String execute(String s1, String s2);

	/* 测试把 java对象传入js处理 */
	public String executebean(Tag s1);

	/* 测试java对象传入js的变量，然后通过函数返回该对象的属性 */
	public String execute1();

	/* 测试java对象传入js的变量，然后通过函数返回该对象 */
	public Tag execute2();

	/* 测试传入java对象，然后在js中修改对象属性，然后返回java对象 */
	public TagC execute3(TagC tagc);

	/* 测试json的处理 */
	public JSONObject execute4(JSONObject jo);

	/* 测试在js中创建java对象返回 */
	public MYzk execute5();

	/* 测试在js中调用java的静态方法 */
	public String execute6();

	/* 测试集合与For Each */
	public String execute7();

	/* 测试map与For Each */
	public String execute8();

	/* 测试java的数组传入js处理 */
	public String execute9();

	/* 测试js中对象继承java类型和js多线程 */
	public String execute10();
}
